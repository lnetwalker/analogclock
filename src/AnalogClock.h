/*
 * AnalogClock.h
 * (c) 2019 by Hartmut Eilers 
 * released under the conditions of the GNU GPL
 * routines to draw an analog clock
 */

#ifndef ANALOG_CLOCK_H
#define ANALOG_CLOCK_H

#include <Arduino.h>
#ifdef OLEDdisplay
#include "oled.h"
#endif
#ifdef Digoledisplay
#include <DigoleSerial.h>
#endif

class AnalogClock
{
public:
    AnalogClock(void);
    #ifdef OLEDdisplay
    void begin(OledDisp *anyOLED,int clockCenterX,int clockCenterY,int radius);
    #endif
    #ifdef Digoledisplay
    void begin(DigoleSerialDisp *anyOLED,int clockCenterX,int clockCenterY,int radius);
    #endif
    void drawDisplay();
    void drawMark(int h);
    void drawSec(int s);
    void drawMin(int m);
    void drawHour(int h, int m);

    
private:
    #ifdef OLEDdisplay
    OledDisp *_myOLED;
    #endif
    #ifdef Digoledisplay
    DigoleSerialDisp *_myOLED;
    #endif
    int _clockCenterX;
    int _clockCenterY;
    int _radius;
    #ifdef smalldisplay
    int korrektur = 12;
    int zeigerbreite = 12;
    #endif
    #ifdef bigdisplay
    int korrektur = 32;
    int zeigerbreite = 22;
    #endif
};

#endif // ANALOG_CLOCK_H
