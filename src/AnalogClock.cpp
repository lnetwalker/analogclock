/*
 * AnalogClock.cpp
 * (c) 2019 by Hartmut Eilers 
 * released under the conditions of the GNU GPL
 * routines to draw an analog clock
 */

#include "AnalogClock.h"

AnalogClock::AnalogClock(void)
{
  
}

#ifdef OLEDdisplay
void AnalogClock::begin(OledDisp *anyOLED,int clockCenterX,int clockCenterY, int radius)
{
  _myOLED=anyOLED;
  _clockCenterX=clockCenterX;
  _clockCenterY=clockCenterY;
  _radius=radius;
}
#endif

#ifdef Digoledisplay
void AnalogClock::begin(DigoleSerialDisp *anyOLED,int clockCenterX,int clockCenterY,int radius)
{
  _myOLED=anyOLED;
  _clockCenterX=clockCenterX;
  _clockCenterY=clockCenterY;
  _radius=radius;
}
#endif

void AnalogClock::drawDisplay()
{
  // Clear screen
  _myOLED->deleteBuffer();
  
  // Draw Clockface
  for (int i=0; i<2; i++)
  {
    _myOLED->drawCircle(_clockCenterX, _clockCenterY, _radius-i);
  }
  for (int i=0; i<3; i++)
  {
    _myOLED->drawCircle(_clockCenterX, _clockCenterY, i);
  }
  
  // Draw a small mark for every hour
  for (int i=0; i<12; i++)
  {
    drawMark(i);
  }  

}

void AnalogClock::drawMark(int h)
{
  float x1, y1, x2, y2;
  
  h=h*30;                                               // every 30 degree
  h=h+270;
  
  x1=(_radius-2)*cos(h*0.0175);
  y1=(_radius-2)*sin(h*0.0175);
  x2=(_radius-5)*cos(h*0.0175);
  y2=(_radius-5)*sin(h*0.0175);
  
  _myOLED->displayLine(x1+_clockCenterX, y1+_clockCenterY, x2+_clockCenterX, y2+_clockCenterY);
}

void AnalogClock::drawSec(int s)
{
  float x1, y1, x2, y2;

  s=s*6;
  s=s+270;
  
  x1=(_radius-2)*cos(s*0.0175);
  y1=(_radius-2)*sin(s*0.0175);
  x2=(_radius-5)*cos(s*0.0175);
  y2=(_radius-5)*sin(s*0.0175);
  
  //if ((s % 5) == 0)
  //  _myOLED->clrLine(x1+_clockCenterX, y1+_clockCenterY, x2+_clockCenterX, y2+_clockCenterY);
  //else
    _myOLED->displayLine(x1+_clockCenterX, y1+_clockCenterY, x2+_clockCenterX, y2+_clockCenterY);
}

void AnalogClock::drawMin(int m)
{
  float x1, y1, x2, y2, x3, y3, x4, y4;

  m=m*6;
  m=m+270;
  
  x1=(_radius-7)*cos(m*0.0175);
  y1=(_radius-7)*sin(m*0.0175);
  x2=3*cos(m*0.0175);
  y2=3*sin(m*0.0175);
  x3=10*cos((m+zeigerbreite)*0.0175);
  y3=10*sin((m+zeigerbreite)*0.0175);
  x4=10*cos((m-zeigerbreite)*0.0175);
  y4=10*sin((m-zeigerbreite)*0.0175);
  
  _myOLED->displayLine(x1+_clockCenterX, y1+_clockCenterY, x3+_clockCenterX, y3+_clockCenterY);
  _myOLED->displayLine(x3+_clockCenterX, y3+_clockCenterY, x2+_clockCenterX, y2+_clockCenterY);
  _myOLED->displayLine(x2+_clockCenterX, y2+_clockCenterY, x4+_clockCenterX, y4+_clockCenterY);
  _myOLED->displayLine(x4+_clockCenterX, y4+_clockCenterY, x1+_clockCenterX, y1+_clockCenterY);
}

void AnalogClock::drawHour(int h, int m)
{
  float x1, y1, x2, y2, x3, y3, x4, y4;

  h=(h*30)+(m/2);
  h=h+270;
  
  x1=(_radius-korrektur)*cos(h*0.0175);
  y1=(_radius-korrektur)*sin(h*0.0175);
  x2=3*cos(h*0.0175);
  y2=3*sin(h*0.0175);
  
  x3=8*cos((h+zeigerbreite)*0.0175);
  y3=8*sin((h+zeigerbreite)*0.0175);
  x4=8*cos((h-zeigerbreite)*0.0175);
  y4=8*sin((h-zeigerbreite)*0.0175);
  
  _myOLED->displayLine(x1+_clockCenterX, y1+_clockCenterY, x3+_clockCenterX, y3+_clockCenterY);
  _myOLED->displayLine(x3+_clockCenterX, y3+_clockCenterY, x2+_clockCenterX, y2+_clockCenterY);
  _myOLED->displayLine(x2+_clockCenterX, y2+_clockCenterY, x4+_clockCenterX, y4+_clockCenterY);
  _myOLED->displayLine(x4+_clockCenterX, y4+_clockCenterY, x1+_clockCenterX, y1+_clockCenterY);
}
