/*
 * Clock.ino
 * (c) 2019 by Hartmut Eilers 
 * released under the conditions of the GNU GPL
 * 
 * Demo program that displays an analog clock on the cheap
 * I2C OLED SDD1306 or SH1106 controller displays
 * 
 * enhanced to work with the Digole display from the
 * Multifruitpi project
 */

#undef DEBUG_SERIAL

// Board selection
#define LyraT                                                       // Espressif LyraT v4.3 board
#undef ESP8266                                                      // Nodemcu 1.0
#undef ESP32                                                        // generic ESP32 board

/* set the correct defines here, or use eg
 * build_flags = -DOLEDdisplay 
 * or
 * build_flags = -DDigoledisplay
 * in your platform.ini file
 * for OLED display you need to set the SCREENHEIGHT according to your display
*/

// Display selection
//#define OLEDdisplay
//#define Digoledisplay

#include <NTPClient.h>                                              // get realtime via NTP
#include <WiFiUdp.h>                                                // UDP networking for NTP
#ifdef ESP8266
#include <ESP8266WiFi.h>
#endif

#ifdef LyraT
#include <WiFi.h>
#endif

#ifdef OLEDdisplay
#include <Wire.h>                                                   // I2C driver
#include "oled.h"                                                   // driver for OLED Display

// set your configuration data here
#ifdef ESP8266
#define I2C_SCL 5                                                   // I2C Setup: use GPIO 4 and 5 for SDA and SCL
#define I2C_SDA 4                                                   // these are D1 and D2 of the nodemcu board
#endif

#ifdef LyraT
#define I2C_SCL 23                                                  // I2C Setup: use GPIO 18 and 23 for SDA and SCL
#define I2C_SDA 18                                                  // these are on I2C header on LyraT board
#endif

#define smalldisplay
#define SCREENHEIGHT 64                                             // for the OLEDS you can use 32 or 64 lines
#define SCREENWIDTH 128
#define SDD1306 0
#define SH1106  2

#endif

#ifdef Digoledisplay
#include <DigoleSerial.h>                                           // Display Lib

#define SCREENWIDTH 320
#define SCREENHEIGHT 240
#define bigdisplay

// SPI settings 
// the defines ESP32 and ESP8266 are set by the compiler based
// on the platformio project settings. Both defines are used
// throughout this code to take care for proper SPI usage.

#ifdef ESP32
#define  SPI2_MOSI  26            // GPIO26 as MOSI for SPI2
#define  SPI2_CLK  25             // GPIO25 as CLK for SPI2
#define  SPI2_MISO  27            // GPIO27 as MISO for SPI2
#define  SPI2_CS_DISP   2         // GPIO02 as the CS for Display on SPI2
#endif

#ifdef ESP8266
#define  SPI2_MOSI  13            // GPIO13 as MOSI for SPI
#define  SPI2_CLK  14             // GPIO14 as CLK for SPI
#define  SPI2_MISO  12            // GPIO12 as MISO for SPI
#define  SPI2_CS_DISP   0         // GPIO00 as the CS for Display on SPI
#endif

#endif

#include "AnalogClock.h"                                            // the analog clock

#define TIMEZONE 1                                                  // timediff to UTC

#define SSID "hucky.net"
#define PASS "internet4hucky"
WiFiClient clockClient;                                             // clockClient


// end of configuration

// time related settings
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);
int clockCenterX=int (SCREENHEIGHT/2);
int clockCenterY=int (SCREENHEIGHT/2);
int radius=int (SCREENHEIGHT/2)-1;
int old_hour,old_minute;

// Display
#ifdef OLEDdisplay
OledDisp myOLED(SH1106,SCREENWIDTH,SCREENHEIGHT),*P;               // define new Display 
#endif
#ifdef Digoledisplay
//--------SPI setup
DigoleSerialDisp myOLED(SPI2_MOSI,SPI2_CLK,SPI2_CS_DISP,SPI2_MISO),*P; // define new Display 

#endif
AnalogClock myClock = AnalogClock();

void setup() {
  Serial.begin(115200);                                             // serial debug console
  WiFi.mode(WIFI_STA);                                              // only wifi station mode
  WiFi.begin(SSID,PASS);                                            // start wifi
  Serial.println("start WiFi");
  while (WiFi.status() != WL_CONNECTED) {                           // wait for connection
    delay(50);
    Serial.print("*");
  }
  Serial.println("done");

  #ifdef OLEDdisplay
  Wire.begin(I2C_SDA, I2C_SCL);                                     // Initialize I2C 
  
  myOLED.init_OLED();                                               // init display                            
  myOLED.reset_display();
  #endif

  #ifdef Digoledisplay
  myOLED.begin();
  myOLED.displayConfig(0);
  myOLED.setRot90();
  myOLED.backLightBrightness(100);
  myOLED.setBgColor(0x00);                                          // clear screen
  myOLED.setDrawWindow(0,0,SCREENWIDTH,SCREENHEIGHT);
  myOLED.cleanDrawWindow();
  myOLED.setColor(1);                                               // black
  myOLED.setMode('C');                                              // screen mode copy!
  myOLED.drawStr(10,7, "Digole Display test...native");
  delay(1000);
  myOLED.cleanDrawWindow();
  #endif

  P=&myOLED;
  myClock.begin(P,clockCenterX,clockCenterY,radius);
  delay(250);
  myClock.drawDisplay();
  delay(250);
  timeClient.begin();                                               // Time   
  old_hour=0;
  old_minute=0;
}

void loop() {
  timeClient.update();
  // get the time
  int Hours = timeClient.getHours()+TIMEZONE;                       // hours is in UTC, so add some hours to adapt timezone
  if ( Hours >= 24 ) { Hours=Hours - 24 ; }
  int Minutes = timeClient.getMinutes();
  String minuteStr = Minutes < 10 ? "0" + String(Minutes) : String(Minutes);
  if ( old_minute != Minutes ) {
    Serial.print("x");
    #ifdef Digoledisplay
    myOLED.cleanDrawWindow();
    #endif
    myClock.drawDisplay();
    myClock.drawMin(Minutes);
    myClock.drawHour(Hours, Minutes);
    myOLED.displayBuffer();
    old_minute=Minutes;
  }
  Serial.print(".");
}
