# Arduino AnalogClock library

Arduino library to display a small analog clock on cheap OLED or the Digole displays.

Tested with ESP8266/nodemcu, ESP32/nodemcu, ESP01 and Espressif LyraT board.

Digole Display showing the clock

![pcf8574 with keypad](./clockDigole.JPG)

and the same with a 128x32 pixel OLED

![pcf8574 with keypad](./clockSmallOLED.jpg)

To use the clock you need either the OLED or the Digole Display library. You find them here:

* OLED: https://gitlab.com/lnetwalker/oled
* Digole: https://gitlab.com/lnetwalker/digoledisplay

When using the OLED library use the build_flags: -DOLEDdisplay -Dsmalldisplay,

for the Disgole display use: -DDigoledisplay -Dbigdisplay

smalldisplay and bigdisplay are used to set corrections and dimensions depending on the size of the display. You can use them how you like, they are not depending on the display type.

Please look at `src/Clock.ino` on how to use the library.

possible platformio.ini:

    [env:nodemcuv2]
    platform = espressif8266
    board = nodemcuv2
    framework = arduino

    [env:esp32dev]
    platform = espressif32
    board = esp32dev
    framework = arduino

    lib_deps =
        https://gitlab.com/lnetwalker/oled.git
        https://gitlab.com/lnetwalker/digoledisplay.git
        https://github.com/arduino-libraries/NTPClient

    build_flags = -DOLEDdisplay -Dsmalldisplay

    upload_port = COM4
    monitor_port = COM4
    monitor_speed = 115200

